# Ovidentia CMS 6.0 - Information Disclosure on FileManager content

Incorrect Access Control in FileManager in Ovidentia CMS 6.0 allows remote unauthenticated users to view and download content (information disclosure) in the upload directory via path traversal. Tested on version 6.0, this version is vulnerable.


## Information disclosure Ovidentia CMS 6.0

We have discovered incorrect access control on the FileManager content in Ovidentia CMS 6.0 using path traversal (not sure if this is the right attack vector?).
This allows a remote unauthenticad user to view and download content (information disclosure) on the upload directory.


## Proof of Concept (PoC)
In this PoC we Ovidentia CMS 6.0 is hosted on an Windows 7 Professional (build 7601) running Apache httpd 2.4.9 ((Win32) PHP/5.5.12) on port 8080 in the directory 'php'


First I created an uploads directory in the file manager.<br/>
![bigint](images/IMAGE_1.png)<br/>

Now I set the permissions so only users of the administrators group can upload files,<br/> 
but no users should be able to download content of the directory uploads. Let alone unauthenticated users.<br/>
![bigint](images/IMAGE_2.png)<br/>

I opened an incognito browser window so no login info or cache is used.<br/>
![bigint](images/IMAGE_3.png)<br/>

To verify I entered wrong login credentials.<br/>
Login ID: abc<br/>
Password: def<br/>
![bigint](images/IMAGE_4.png)<br/>

Next, using the incognito browser window, I navigated to the Ovidentia Filemanager directory where the uploads are stored.<br/>
url: http://IP:8080/php/filemanager<br/>
As can be seen in the screenshot below we have read access to the filemanager directory.<br/>
![bigint](images/IMAGE_5.png)<br/>

In the filemanager I navigated to:<br/>
http://IP:8080/php/filemanager/collectives/DG0/uploads<br/>
We can view the content of the uploads directory.<br/>
![bigint](images/IMAGE_6.png)<br/>

We also have read access to the files. In the screenshot below I opened the sensitive_file.conf file.<br/>
![bigint](images/IMAGE_7.png)<br/>

We can also download files. In the screenshot below I downloaded the alba.exe file.<br/>
![bigint](images/IMAGE_8.png)<br/>


## Usage
Use the above only in educational purposes or during an official pentest. 

## Authors and acknowledgment
Thanks to Julien Steins for helping me discovering this vulnerability.

## Project status
I haven't further discovered the permissions on other directories or use cases using other attack vectors, happy to do so.
